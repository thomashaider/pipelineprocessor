package emich.ilit.corpus.util;


import static org.uimafit.factory.AnalysisEngineFactory.*;
import static org.uimafit.factory.CollectionReaderFactory.*;
import static org.uimafit.pipeline.SimplePipeline.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.impl.XmiCasSerializer;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.uimafit.component.xwriter.CASDumpWriter;
import org.uimafit.pipeline.JCasIterable;
import org.uimafit.component.xwriter.XWriter;

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.resources.DKProContext;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Lemma;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.BreakIteratorSegmenter;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordParser;

import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.factory.AnalysisEngineFactory;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.io.tei.TEIReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import de.tudarmstadt.ukp.dkpro.core.io.xml.*; 
import de.tudarmstadt.ukp.dkpro.core.testing.AssertAnnotations;
import de.tudarmstadt.ukp.dkpro.core.testing.TestRunner;
import edu.stanford.nlp.io.StringOutputStream;

import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.XmlCasSerializer;

public class GenerateAEDescription {
    
	public static void main(String[] args) throws Exception
    	{
		StanfordLemmatizer("/usr/share/uima/descriptors/");
		//StanfordSegmenter();
		//OpenNlpPosTagger();
    	}
	
	// does not work; generates files that cannot be read by uima
	// If you figure out, how to generate generics with this, let me know
	public static void generate(String className) throws ResourceInitializationException, FileNotFoundException, SAXException, IOException, ClassNotFoundException{
		String theClassName = className + ".class";
		Class aClass = Class.forName(className).getClass();
		AnalysisEngineDescription analysisEngine = 
			    AnalysisEngineFactory.createPrimitiveDescription(aClass
			    		);
		
			analysisEngine.toXML(new FileOutputStream("/home/tom/Documents/" + className + ".xml"));
			System.out.println("Done");
	}
	
		public static void StanfordLemmatizer(String directoryWhereToSaveTo) throws ResourceInitializationException, FileNotFoundException, SAXException, IOException{
			AnalysisEngineDescription analysisEngine = 
				    AnalysisEngineFactory.createPrimitiveDescription(StanfordLemmatizer.class
				    		);
			
				analysisEngine.toXML(new FileOutputStream(directoryWhereToSaveTo + "StanfordLemmatizer.xml"));
				System.out.println("Done");
		}
		
		public static void stanfordSegmenter() throws ResourceInitializationException, FileNotFoundException, SAXException, IOException{
			AnalysisEngineDescription analysisEngine = 
				    AnalysisEngineFactory.createPrimitiveDescription(StanfordSegmenter.class
				    		);
			
				analysisEngine.toXML(new FileOutputStream("/home/tom/Documents/StanfordSegmenter.xml"));
				System.out.println("Done");
		}
	
		public static void OpenNlpPosTagger() throws ResourceInitializationException, FileNotFoundException, SAXException, IOException{
			AnalysisEngineDescription analysisEngine = 
				    AnalysisEngineFactory.createPrimitiveDescription(OpenNlpPosTagger.class,
				    		OpenNlpPosTagger.PARAM_LANGUAGE, "en"
				    		);
			
				analysisEngine.toXML(new FileOutputStream("/home/tom/Documents/OpenNlpPosTagger.xml"));
				System.out.println("Done");
		}
	
    }